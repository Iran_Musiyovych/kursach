<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Places
{
    public function getOwner(){
        return DB::select('select distinct(owner) from park.places order by owner');
    }
    public function getCar(){
        return DB::select('select distinct(car) from park.places order by car');
    }

    public function getPlaces($owner, $car){

        $query = DB::table('places');
        $query->select('place_id', 'owner', 'car', 'cost')
            ->orderBy('place_id');
        if($owner){
            $query->where('owner', '=', $owner);
        }
        if($car){
            $query->where('car', '=', $car);
        }
        $places = $query->get();
        return $places;
    }

    public function getPlaceByID($id){
        if(!$id) return null;
        $group = DB::table('places')
            ->select('*')
            ->where('place_id', $id)
            ->get()->first();
        return $group;
    }
}
