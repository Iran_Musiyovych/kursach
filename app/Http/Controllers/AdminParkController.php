<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Place;
use Illuminate\Support\Facades\Redirect;

class AdminParkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::get();
        return view('admin.parking.list', ['places' => $places]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parking.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $owner = $request->input('owner');
        $car = $request->input('car');
        $cost = $request->input('cost');
        $place = new Place();
        $place->owner = $owner;
        $place->car = $car;
        $place->cost = $cost;
        $place->save();
        return Redirect::to('/admin/park');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $places = Place::where('place_id', $id)->first();
        return view('admin.parking.edit',
            ['places' => $places]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $place = Place::where('place_id', $id)->first();
        $place->owner = $request->input('owner');
        $place->car = $request->input('car');
        $place->cost = $request->input('cost');
        $place->save();
        return Redirect::to('/admin/park');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Place::destroy($id);
        return Redirect::to('/admin/park');
    }
}
