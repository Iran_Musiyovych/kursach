<?php

namespace App\Http\Controllers;
use App\Models\Places;
use Illuminate\Http\Request;

class ParkController extends Controller
{
    public function index(Request $request){

        $owner = $request->input('owner', null);
        $car = $request->input('car', null);

        $model_place = new Places();

        $places = $model_place->getPlaces($owner, $car);
        return view('app.list',
               ['place' => $places,
                'owner_selected' => $owner,
                'car_selected' => $car]
        );
    }

    public function places($id){
        $model_place = new Places();
        $places = $model_place->getPlaceByID($id);
        return view('app.parking')->with('places', $places);
    }

    public function adminplaces($id){
        $model_place = new Places();
        $places = $model_place->getPlaceByID($id);
        return view('admin.parking.parking')->with('places', $places);
    }
}
