@extends('admin.layout')

<style type="text/css">
    label {
        min-width: 100px;
        display: inline-block;
        color: mediumvioletred;
    }
</style>

@section('content')
    <h2 style="color: white">Parking place adding</h2>
    <form action="/admin/park" method="POST">
        {{ csrf_field() }}
        <label>Owner</label>
        <input name="owner">
        <br/><br/>
        <label>Car</label>
        <input name="car">
        <br/><br/>
        <label>Price</label>
        <input name="cost">
        <br/><br/>
        <input type="submit" value="Save">
    </form>
@endsection
