@extends('admin.layout')
@section('content')
    <h2 style="color: white;">Parking places</h2>
    <table id="table">
        <th id="table_cell">Place</th>
        <th id="table_cell">Owner</th>
        <th id="table_cell">Car</th>
        <th id="table_cell">Price</th>
        <th id="table_cell">Options</th>
        @foreach ($places as $place)
            <tr>
                <td id="table_cell">
                    <a href="/admin/parks/{{ $place->place_id }}">
                        {{ $place->place_id }}
                    </a>
                </td>
                <td id="table_cell">{{ $place->owner}}</td>
                <td id="table_cell">{{ $place->car }}</td>
                <td id="table_cell">{{ $place->cost }}</td>
                <td id="table_cell">
                    <p><a href="/admin/park/{{ $place->place_id }}/edit">Edit</a></p>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/park/{{ $place->place_id}}"method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button>Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
