@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 100px;
        display: inline-block;
        color: mediumvioletred;
    }
</style>
@section('content')
    <h2 style="color: white;">Parking place editing</h2>
    <form action="/admin/park/{{ $places->place_id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <label>Owner</label>
        <input name="owner" value="{{$places->owner}}">
        <br/><br/>
        <label>Car</label>
        <input name="car" value="{{$places->car}}">
        <br/><br/>
        <label>Price</label>
        <input name="cost" value="{{$places->cost}}">
        <br/><br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
