<html>
<head>
    <title>Parking information system</title>
</head>
<style>
    body{
        height: 500pt;
        background-color: lavender;
        background-color: thistle;
    }
    #table{
        border: darkmagenta solid 5px;
        padding: 10px;
        background-color: white;
    }
    #table_cell{
        border: mediumvioletred solid 2px;
        padding: 10px;
        text-align: center;
    }
    .container{
        color: white;
        padding: 0px 0px 0px 20px;
        text-align: center;
    }
</style>
<div class="container"  style="display: flex; justify-content: center; align-items: center">
</div>
<div class="leftnav" style="float: left; width: 150px; height: 100%; margin-right:
30px;">
    <b id="text" style="color: darkmagenta; text-shadow:#4a5568; font-size: 21px;">Parking</b>
    <ul style="color: darkmagenta; text-shadow:#4a5568; font-size: 21px;">
        <li ><a href="/admin/park" id="text">List</a></li>
        <li><a href="/admin/park/create" id="text">Add</a></li>
    </ul>
    <br/>
    <ul style="color: darkmagenta; text-shadow:#4a5568; font-size: 21px;">
        <li><a href="/park" id="text">Back</a></li>
        <li><a href="/" id="text">Main menu</a></li>
    </ul>
</div>
<div>
    @yield('content')
</div>

