@extends('layouts.layout')

<?php
$model_places = new App\Models\Places();
$car = $model_places->getCar();
$owner = $model_places->getOwner();
?>

@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center; ">
        <form method="get" action="/park">
            <span>Owner :</span>
            <select name="owner" id="table_cell">
                <option value="0">All</option>
                @foreach($owner as $o)
                    <option value="{{ $o->owner }}"
                        {{ ( $o->owner == $owner_selected ) ? 'selected' : '' }}>
                        {{ $o->owner }}
                    </option>
                @endforeach
            </select>

            <span>Car :</span>
            <select name="car" id="table_cell">
                <option value="0">All</option>
                @foreach($car as $c)
                    <option value="{{ $c->car }}"
                        {{ ( $c->car == $car_selected ) ? 'selected' : '' }}>
                        {{ $c->car }}
                    </option>
                @endforeach
            </select>
            <div class="container"  style="display: flex; justify-content: center; align-items: center;">
                <p></p>
            <a href="/admin/park" id="text">List</a>
            </div>
            <div class="container"  style="display: flex; justify-content: center; align-items: center">
            <p><input type="submit" id="table_cell" value="submit"/></p>
            </div>
            <div class="container"  style="display: flex; justify-content: center; align-items: center">
                <p><h2>Parking places</h2></p>
            </div>
        </form>
    </div>

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <table id="table">
            <th id="table_cell">Place</th>
            <th id="table_cell">Owner</th>
            <th id="table_cell">Car</th>
            <th id="table_cell">Price</th>

            @foreach ($place as $places)
                <tr>
                    <td id="table_cell">
                        <a href="/park/{{ $places->place_id }}">
                            {{ $places->place_id}}
                        </a>
                    </td>
                    <td id="table_cell">{{ $places->owner}}</td>
                    <td id="table_cell">{{ $places->car }}</td>
                    <td id="table_cell">{{ $places->cost }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
