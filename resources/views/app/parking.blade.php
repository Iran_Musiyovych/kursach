@extends('layouts.layout')

@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <p>Information about parking place: {{ $places->place_id }}</p>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <table id="table">
            <th id="table_cell">Owner</th>
            <th id="table_cell">Car</th>
            <th id="table_cell">Price</th>
            <tr>
                <td id="table_cell"> {{ $places->owner }}</td>
                <td id="table_cell"> {{ $places->car }}</td>
                <td id="table_cell"> {{ $places->cost }}</td>
            </tr>
        </table>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <p></p>
        <a href="/park">Show all parking places</a>
    </div>
@endsection
